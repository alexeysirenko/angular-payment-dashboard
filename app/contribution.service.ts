import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ContributionService {

    private url = "//localhost:3004/contributions";

    constructor(private http: Http) {}

    getContributions(start: number, limit: number, filter?) {   
        console.log('Loading ' + limit + ' contrinutions starting from ' + start + ' with filter ' + JSON.stringify(filter));
        let url = this.url + '?_start=' + start + '&_limit=' + limit;
        if (filter && filter.id)
            url += '&id=' + filter.id;        
        return this.http.get(url)
            .map(response => { 
                return {
                    contributions: response.json(),
                    totalCount: +response.headers.get('X-Total-Count')
                }                 
            })
    }   
}