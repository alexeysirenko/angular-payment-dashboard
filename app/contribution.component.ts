import {Component, OnInit} from '@angular/core';
import {ContributionService} from './contribution.service';

@Component({
    selector: 'contributions',
    templateUrl: '/app/contribution.component.html'    
})
export class ContributionComponent implements OnInit {

    readonly pageSize = 5;

    totalCount: number = 0;

    contributions: any[];

    private currentFilter;

    isLoadingContributions = false;

    constructor(private contributionService: ContributionService) {}       

    ngOnInit() {
        this.getContributions(0, this.currentFilter);
    }

    getContributions(start: number, filter?) {
        console.log("Reloading contributions with filter " + JSON.stringify(filter));
        this.isLoadingContributions = true;
        //this.contributions = [];
        this.currentFilter = filter;
        this.contributionService.getContributions(start, this.pageSize, filter)
            .subscribe(
                response => {
                    this.contributions = response.contributions;
                    this.totalCount = response.totalCount;
                    console.log("Got " + this.contributions.length + " contributions. Total count: " + this.totalCount);
                },
                null,
                () => this.isLoadingContributions = false
            )
    }

    onPageChanged(page: number) {
        let startIndex = this.pageSize * (page - 1);
        this.getContributions(startIndex, this.currentFilter);
    }
}