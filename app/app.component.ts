import { Component } from '@angular/core';
@Component({
  selector: 'my-app',
  template: `
    <div class="container">
      <contributions></contributions>
    </div>  
  `
})
export class AppComponent { }
