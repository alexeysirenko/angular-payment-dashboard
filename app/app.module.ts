import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent }   from './app.component';

import { SpinnerComponent } from './spinner.component';
import { PaginationComponent } from './pagination.component';
import { ContributionComponent } from './contribution.component';

import { ContributionService } from './contribution.service'

@NgModule({
  imports:      [ 
    BrowserModule,
    HttpModule,
    JsonpModule
  ],
  declarations: [ 
    AppComponent,
    SpinnerComponent,
    PaginationComponent,
    ContributionComponent
  ],
  providers: [
    ContributionService
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
